import { IsEmail, IsNotEmpty, IsString, MinLength } from "class-validator";

export class VerificarDto {
    @IsEmail()
    @IsNotEmpty()
    @IsString()
    email: string;

    @IsString()
    @IsNotEmpty()
    @MinLength(4, { message: 'Debe contener al menos cuatro caracteres'})
    password: string;
}