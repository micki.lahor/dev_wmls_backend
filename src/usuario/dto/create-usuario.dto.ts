import { IsAlphanumeric, IsEmail, IsNotEmpty, IsString, MinLength } from "class-validator";

export class CreateUsuarioDto {
    @IsString()
    @IsNotEmpty()
    @MinLength(5, { 
        message: 'El nombre debe contener al menos 5 caracteres'})
    nombre: string;
    @IsString()
    @IsNotEmpty()
    @MinLength(5, { 
        message: 'El nombre de usuario debe contener al menos 5 caracteres'})
    @IsAlphanumeric(null, { message: 'Solo se permiten letras y numeros' })
    usuario: string;
    @IsString()
    @IsEmail(null, { message: 'Ingrese un correo electrónico valido' })
    @IsNotEmpty()
    email: string;
    @IsString()
    @IsNotEmpty()
    @MinLength(8, { message: 'La contraseña debe contener al menos 8 caracteres'})
    password: string;
}
